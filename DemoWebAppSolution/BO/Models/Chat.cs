﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Chat
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Age { get; set; }
        public string Couleur { get; set; }

        public List<Ingredient> Ingredients { get; set; } = new List<Ingredient>(); // Valeur par défaut

        public Continent Continent { get; set; }

        public Chat()
        {
            Debug.WriteLine("Nouvelle instance de chat.");
        }
    }
}
