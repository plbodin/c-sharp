﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Services
{
    public class ChatService
    {
        private List<Chat> _listeChats;
        private List<Ingredient> _listeIngredients;
        private List<Continent> _listeContinents;

        #region Singleton

        private ChatService()
        {
            this._listeChats = GetMeuteDeChats();
            this._listeIngredients = new List<Ingredient>
            {
                new Ingredient { Id = 1, Nom = "Souris" },
                new Ingredient { Id = 2, Nom = "Mulot" },
                new Ingredient { Id = 3, Nom = "Croquettes" },
                new Ingredient { Id = 4, Nom = "Chewing-gum" },
                new Ingredient { Id = 5, Nom = "Pâté" },
                new Ingredient { Id = 6, Nom = "Musaraigne" }
            };

            this._listeContinents = new List<Continent>
            {
                new Continent { Id = 1, Nom = "Europe" },
                new Continent { Id = 2, Nom = "Afrique" },
                new Continent { Id = 3, Nom = "Asie" },
                new Continent { Id = 4, Nom = "Amérique" },
                new Continent { Id = 5, Nom = "Océanie" }
            };
        }

        private static ChatService _instance;

        private static List<Chat> GetMeuteDeChats()
        {
            var i = 1;
            return new List<Chat>
            {
                new Chat{Id=i++,Nom = "Felix",Age = 3,Couleur = "Roux"},
                new Chat{Id=i++,Nom = "Minette",Age = 1,Couleur = "Noire"},
                new Chat{Id=i++,Nom = "Miss",Age = 10,Couleur = "Blanche"},
                new Chat{Id=i++,Nom = "Garfield",Age = 6,Couleur = "Gris"},
                new Chat{Id=i++,Nom = "Chatran",Age = 4,Couleur = "Fauve"},
                new Chat{Id=i++,Nom = "Minou",Age = 2,Couleur = "Blanc"},
                new Chat{Id=i,Nom = "Bichette",Age = 12,Couleur = "Rousse"}
            };
        }
        public static ChatService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ChatService();
                }
                return _instance;
            }
        }
        #endregion

        public List<Ingredient> GetIngredients()
        {
            return _listeIngredients;
        }

        public List<Continent> GetContinents()
        {
            return _listeContinents;
        }

        public List<Chat> GetChats()
        {
            return _listeChats;
        }

        /// <summary>
        /// Retourne un chat selon le prédicat donné.
        /// </summary>
        /// <param name="filter">Prédicat</param>
        /// <returns></returns>
        public Chat GetChat(Func<Chat, bool> filter)
        {
            return this._listeChats.FirstOrDefault(filter);
        }

        public Chat GetChatById(int id)
        {
            return this.GetChat(c => c.Id == id);
        }

        public void RemoveChat(Chat chat)
        {
            this._listeChats.Remove(chat);
        }

        public void AddChat(Chat chat)
        {
            chat.Id = this._listeChats.Max(c => c.Id) + 1;

            this._listeChats.Add(chat);
        }

        public void Save()
        {
            // Ne fait rien, tous les chats sont déjà en mémoire
        }
    }
}
