﻿using BO.Models;
using BO.Services;
using DemoWebApp.Mappers;
using DemoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoWebApp.Controllers
{
    public class ChatController : Controller
    {
        // GET: Chat
        public ActionResult Index()
        {
            List<Chat> chats = ChatService.Instance.GetChats();

            return View(chats);
        }

        // GET: Chat/Details/5
        public ActionResult Details(int id)
        {
            Chat chat = ChatService.Instance.GetChatById(id);
            return View(chat);
        }


        // GET: Chat/Delete/5
        public ActionResult Delete(int id)
        {
            Chat chat = ChatService.Instance.GetChatById(id);
            return View(chat);
        }

        // POST: Chat/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Chat chat = ChatService.Instance.GetChatById(id);

                ChatService.Instance.RemoveChat(chat);

                ChatService.Instance.Save();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Chat/Create
        public ActionResult Create()
        {
            ChatViewModel chat = new ChatViewModel();
            chat.FillListes();
            return View(chat);
        }

        [HttpPost]
        public ActionResult VerifyCatName(string Nom, int? Id)
        {
            Chat chat = ChatService.Instance.GetChat(c => c.Nom == Nom && c.Id != Id);

            if (chat == null)
            {
                return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        public ActionResult VerifyCatIngredients(string IngredientsIds, int? Id)
        {
            // On reçoit les ingrédients sélectionnés, dans un string, avec un séparateur ','
            string[] ingredientsIds = IngredientsIds.Split(',');

            Chat chatAvecCesIngredients =
                 ChatService.Instance.GetChat(c =>
                  // Compare le nombre d'ingrédients
                 c.Ingredients.Count == ingredientsIds.Count() 
                 // On filtre pour ne pas rechercher le chat en cours d'édition
                 && c.Id != Id
                 // Compare l'intersection
                 && c.Ingredients.Select(i => "" + i.Id).Intersect(ingredientsIds).Count() == ingredientsIds.Count()
                
             );

            // Si on ne trouve pas de chat pour ces ingrédients, on retourne true pour indiquer qu'il n'y a pas d'erreur
            return Json(chatAvecCesIngredients == null);
        }

        // POST: Chat/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ChatViewModel vm)
        {
            try
            {
                // Ajout d'une erreur dans le model state si l'age est négatif
                if (vm.Age < 0)
                {
                    ModelState.AddModelError(nameof(ChatViewModel.Age), "L'âge doit être > 0.");
                }

                if (!ModelState.IsValid)
                {
                    vm.FillListes();
                    return View(vm);
                }

                Chat chat = new Chat();

                // Mapping du ViewModel dans le Model
                ChatViewModelMapper.Map(vm, chat);

                // Envoi du Model au service métier
                ChatService.Instance.AddChat(chat);

                ChatService.Instance.Save();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Si un cas d'erreur, les listes vont être vides, il faut les réinitialiser
                vm.FillListes();

                return View(vm);
            }
        }

        // GET: Chat/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            // Récupération du chat en base
            Chat chat = ChatService.Instance.GetChatById(id);

            // Création du view model a envoyer à la vue
            ChatViewModel vm = new ChatViewModel();

            // Mapping du model dans le view model
            ChatViewModelMapper.Map(chat, vm);

            vm.FillListes();

            // Envoi à la vue du view model
            return View(vm);
        }

        // POST: Chat/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ChatViewModel vm)
        {
            // Le chat reçu ici n'a pas de référence. si on le modifie, il ne sera pas modifié en base.            
            if (!ModelState.IsValid)
            {
                vm.FillListes();
                return View(vm);
            }

            try
            {
                // On récupère le chat depuis le service métier pour travailler avec sa référence
                Chat chatEnBase = ChatService.Instance.GetChatById(vm.Id);

                ChatViewModelMapper.Map(vm, chatEnBase);

                // Persistance en base de données
                ChatService.Instance.Save();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                vm.FillListes();

                return View(vm);
            }
        }
    }
}
