﻿using DemoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoWebApp.Controllers
{
    public class GarageController : Controller
    {
        public ActionResult Index()
        {
            this.ViewBag.DateDuJour = DateTime.Now.ToString("dd/MM/yyyy");

            GarageViewModel vm = new GarageViewModel();
            vm.Nom = "Le garage du coin";
            vm.NombreVoitures = 12;

            return View(vm); // Retourne la vue "Index.cshtml" du dossier "~/Views/Garage/"
        }

        [Route("Voitures/{proprietaire}")]
        public ActionResult ListeVoitures(string proprietaire)
        {
            ViewBag.Nom = proprietaire;
            return View();
        }
    }
}