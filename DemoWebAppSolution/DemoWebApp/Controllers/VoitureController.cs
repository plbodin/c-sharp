﻿using DemoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoWebApp.Controllers
{
    public class VoitureController : Controller
    {
        private static List<VoitureViewModel> _voitures = new List<VoitureViewModel>
            {
                new VoitureViewModel {
                    Id = 1,
                    Marque = "Citroen",
                    Modele = "Saxo",
                    PremiereMiseEnCirculation = new DateTime(2001, 2, 13)
                },
                 new VoitureViewModel {
                    Id = 2,
                    Marque = "Peugeot",
                    Modele = "308",
                    PremiereMiseEnCirculation = new DateTime(2011, 12, 6)
                },
            };

        // GET: Voiture
        public ActionResult Index()
        {
            return View(_voitures);
        }

        // GET: Voiture/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Voiture/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Voiture/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Voiture/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            VoitureViewModel voiture = _voitures.FirstOrDefault(v => v.Id == id);
            return View(voiture);
        }

        // POST: Voiture/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VoitureViewModel vm)
        {
            // Si le modèle n'est pas valide
            // on le retourne avec la vue associée
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            try
            {
                VoitureViewModel voiture = _voitures.FirstOrDefault(v => v.Id == vm.Id);

                // Mapping des champs reçus par le front
                voiture.Marque = vm.Marque;
                voiture.Modele = vm.Modele;
                voiture.PremiereMiseEnCirculation = vm.PremiereMiseEnCirculation;
                
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Voiture/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Voiture/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
