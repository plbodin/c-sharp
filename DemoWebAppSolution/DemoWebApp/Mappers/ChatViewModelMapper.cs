﻿using BO.Models;
using BO.Services;
using DemoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebApp.Mappers
{
    public class ChatViewModelMapper
    {
        /// <summary>
        /// Mappe le chat "model" vers le view model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="vm"></param>
        public static void Map(Chat model, ChatViewModel vm)
        {
            vm.Age = model.Age;
            vm.Nom = model.Nom;
            vm.Couleur = model.Couleur;

            if (model.Continent != null)
            {
                vm.ContinentId = "" + model.Continent.Id;
            }

            vm.IngredientsIds = model.Ingredients.Select(i => "" + i.Id).ToArray();
        }

        /// <summary>
        /// Mappe le view model vers le model
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="chat"></param>
        public static void Map(ChatViewModel vm, Chat chat)
        {
            // Mapping des champs
            chat.Nom = vm.Nom;
            chat.Couleur = vm.Couleur;
            chat.Age = vm.Age;

            // Récupération de tous les ingrédients de la base qui sont sélectionnés
            if (vm.IngredientsIds == null)
            {
                chat.Ingredients.Clear();
            }
            else
            {
                chat.Ingredients = ChatService.Instance.GetIngredients()
                                                       .Where(i => vm.IngredientsIds.Contains("" + i.Id))
                                                       .ToList();
            }

            // On récupère le continent depuis le service métier
            chat.Continent = ChatService.Instance.GetContinents().FirstOrDefault(c => "" + c.Id == vm.ContinentId);
        }
    }
}