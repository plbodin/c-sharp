﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoWebApp.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static HtmlString CustomSubmit(this HtmlHelper helper, string label)
        {
            return new HtmlString($"<button class='btn btn-success' type='submit'>{label}</button>");
        }
    }
}