﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoWebApp.Models
{
    public class VoitureViewModel
    {
        public int Id { get; set; }
        public string Modele { get; set; }

        [Required]
        public string Marque { get; set; }
        public DateTime PremiereMiseEnCirculation { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Marque}/{Modele}/{PremiereMiseEnCirculation}";
        }
    }
}