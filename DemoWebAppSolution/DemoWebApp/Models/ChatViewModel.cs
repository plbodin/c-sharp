﻿using BO.Services;
using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoWebApp.Models
{
    public class ChatViewModel
    {
        public int Id { get; set; }

        [Remote(
            "VerifyCatName",
            controller: "Chat",
            AdditionalFields = "Nom,Id",
            HttpMethod = "POST",
            ErrorMessage = "Le nom est déjà pris."),
            ]
        [Required]
        [StringLength(20, MinimumLength = 5)]
        public string Nom { get; set; }
        public int Age { get; set; }

        [Required]
        public string Couleur { get; set; }

        /// <summary>
        /// Contient les identifiants sélectionnés de la liste des ingrédients
        /// </summary>
        [DisplayName("Ingrédients")]
        [MinLength(2, ErrorMessage = "Un chat mange au moins {1} ingrédients")]
        [MaxLength(5, ErrorMessage = "Un chat ne mange pas plus de {1} ingrédients")]
        [Required]
        [Remote(
            "VerifyCatIngredients",
            controller: "Chat",
            AdditionalFields = "IngredientsIds,Id",
            HttpMethod = "POST",
            ErrorMessage = "Une chat existe déjà pour la liste des ingrédients.")
            ]
        public string[] IngredientsIds { get; set; }

        public IEnumerable<SelectListItem> TousLesIngredients { get; set; }

        [DisplayName("Continent")]
        [Required]
        public string ContinentId { get; set; }

        public IEnumerable<SelectListItem> TousLesContinents { get; set; }

        public void FillListes()
        {
            this.TousLesIngredients = ChatService.Instance.GetIngredients().Select(c => new SelectListItem
            {
                Text = c.Nom,
                Value = "" + c.Id
            });

            this.TousLesContinents = ChatService.Instance.GetContinents().Select(c => new SelectListItem
            {
                Text = c.Nom,
                Value = "" + c.Id
            });
        }
    }
}