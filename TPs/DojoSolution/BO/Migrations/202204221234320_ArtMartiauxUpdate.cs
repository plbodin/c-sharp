﻿namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArtMartiauxUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ArtMartials", "Samourai_Id", "dbo.Samourais");
            DropIndex("dbo.ArtMartials", new[] { "Samourai_Id" });
            CreateTable(
                "dbo.ArtMartialSamourais",
                c => new
                    {
                        ArtMartial_Id = c.Int(nullable: false),
                        Samourai_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ArtMartial_Id, t.Samourai_Id })
                .ForeignKey("dbo.ArtMartials", t => t.ArtMartial_Id, cascadeDelete: true)
                .ForeignKey("dbo.Samourais", t => t.Samourai_Id, cascadeDelete: true)
                .Index(t => t.ArtMartial_Id)
                .Index(t => t.Samourai_Id);
            
            DropColumn("dbo.ArtMartials", "Samourai_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ArtMartials", "Samourai_Id", c => c.Int());
            DropForeignKey("dbo.ArtMartialSamourais", "Samourai_Id", "dbo.Samourais");
            DropForeignKey("dbo.ArtMartialSamourais", "ArtMartial_Id", "dbo.ArtMartials");
            DropIndex("dbo.ArtMartialSamourais", new[] { "Samourai_Id" });
            DropIndex("dbo.ArtMartialSamourais", new[] { "ArtMartial_Id" });
            DropTable("dbo.ArtMartialSamourais");
            CreateIndex("dbo.ArtMartials", "Samourai_Id");
            AddForeignKey("dbo.ArtMartials", "Samourai_Id", "dbo.Samourais", "Id");
        }
    }
}
