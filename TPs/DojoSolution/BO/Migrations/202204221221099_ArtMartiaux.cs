﻿namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Core.Metadata.Edm;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Model;

    public partial class ArtMartiaux : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Samourais", new[] { "Arme_Id" });
            //   RenameColumn(table: "dbo.Armes", name: "Arme_Id", newName: "Samourai_Id");

            AddColumn("dbo.Armes", "Samourai_Id", c => new ColumnModel(PrimitiveTypeKind.Int32)
            {
                IsNullable = true
            });

            AddForeignKey("dbo.Armes", "Samourai_Id", "dbo.Samourais");
          

            CreateTable(
                "dbo.ArtMartials",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Nom = c.String(),
                    Samourai_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Samourais", t => t.Samourai_Id)
                .Index(t => t.Samourai_Id);

            CreateIndex("dbo.Armes", "Samourai_Id");

            DropForeignKey("dbo.Samourais", "FK_dbo.Samourais_dbo.Armes_Arme_Id");
            DropColumn("dbo.Samourais", "Arme_Id");
        }

        public override void Down()
        {
            AddColumn("dbo.Samourais", "Arme_Id", c => c.Int());
            DropForeignKey("dbo.ArtMartials", "Samourai_Id", "dbo.Samourais");
            DropIndex("dbo.ArtMartials", new[] { "Samourai_Id" });
            DropIndex("dbo.Armes", new[] { "Samourai_Id" });
            DropTable("dbo.ArtMartials");
            RenameColumn(table: "dbo.Armes", name: "Samourai_Id", newName: "Arme_Id");
            CreateIndex("dbo.Samourais", "Arme_Id");
        }
    }
}
