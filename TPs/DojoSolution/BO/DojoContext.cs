﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public class DojoContext : DbContext
    {
        public DbSet<Arme> Armes { get; set; }
        public DbSet<Samourai> Samourais { get; set; }

        public DojoContext() :
            base(ConfigurationManager.ConnectionStrings["default"].ConnectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //  modelBuilder.Entity<Samourai>().HasOptional(s => s.Arme).WithOptionalDependent(a => a.Samourai);
            modelBuilder.Entity<Arme>().HasOptional(a => a.Samourai)
                .WithOptionalDependent(s => s.Arme).Map(a => a.MapKey("Samourai_Id"));
        }

        public System.Data.Entity.DbSet<BO.Models.ArtMartial> ArtMartials { get; set; }
    }
}
