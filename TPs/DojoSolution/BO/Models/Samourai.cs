﻿using System.Collections.Generic;
using System.ComponentModel;

namespace BO.Models
{
    public class Samourai : BaseEntity
    {
        public int Force { get; set; }
        public virtual Arme Arme { get; set; }

        [DisplayName("Arts martiaux maîtrisés")]
        public virtual List<ArtMartial> ArtMartiaux { get; set; }

        public int Potentiel => (this.Force + (this.Arme?.Degats ?? 0)) * (this.ArtMartiaux.Count + 1);   
    }
}
