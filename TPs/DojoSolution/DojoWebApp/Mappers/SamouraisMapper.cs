﻿using BO;
using BO.Models;
using DojoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DojoWebApp.Mappers
{
    public class SamouraisMapper
    {
        private DojoContext _dojoContext;

        public SamouraisMapper(DojoContext context)
        {
            this._dojoContext = context;
        }

        /// <summary>
        /// Mappe le model dans une nouvelle instance de VM
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DetailSamouraiViewModel Map(Samourai model)
        {
            DetailSamouraiViewModel vm = new DetailSamouraiViewModel();         
            this.Map(model, vm);

            vm.ArmeName = model.Arme?.Nom;
            vm.ArtMartiauxNames = string.Join(", ", model.ArtMartiaux.Select(a => a.Nom));

            return vm;
        }

        /// <summary>
        /// Map le VM vers le M
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="model"></param>
        public void Map(CreateEditSamouraiViewModel vm, Samourai model)
        {
            model.Force = vm.Force;
            model.Nom = vm.Nom;
            model.Arme = this._dojoContext.Armes.FirstOrDefault(a => a.Id == vm.ArmeId);
            model.ArtMartiaux = this._dojoContext.ArtMartials.Where(a => vm.ArtMartiauxId.Contains(a.Id)).ToList();
        }

        /// <summary>
        /// Mappe le M vers le VM
        /// </summary>
        /// <param name="model"></param>
        /// <param name="vm"></param>
        public void Map(Samourai model, SamouraiViewModel vm)
        {
            vm.Id = model.Id;
            vm.Force = model.Force;
            vm.Nom = model.Nom;
            vm.Potentiel = model.Potentiel;
        }

        /// <summary>
        /// Mappe le M vers le VM
        /// </summary>
        /// <param name="model"></param>
        /// <param name="vm"></param>
        public void Map(Samourai model, CreateEditSamouraiViewModel vm)
        {
            this.Map(model, vm as SamouraiViewModel);

            vm.ArtMartiauxId = model.ArtMartiaux.Select(a => a.Id).ToArray();
            vm.ArmeId = model.Arme?.Id;
        }
    }
}