﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BO;
using BO.Models;
using DojoWebApp.Mappers;
using DojoWebApp.Models;

namespace DojoWebApp.Controllers
{
    public class SamouraisController : Controller
    {
        private DojoContext db = new DojoContext();

        private SamouraisMapper samouraisMapper;

        public SamouraisController()
        {
            this.samouraisMapper = new SamouraisMapper(this.db);
        }

        // GET: Samourais
        public ActionResult Index()
        {
            // On fait un premier ToList() sur le DbSet pour ajouter du Linq par la suite.
            // Le ToList() va permettre a EF de construire la liste a partir du DbSet.
            // Pour mémoire, sur un DbSet, le Linq utilisé est appelé LinqToSql, et EF va tenté de "transformer"
            // La requête Linq en SQL
            List<DetailSamouraiViewModel> vm = db.Samourais.ToList().Select(s => this.samouraisMapper.Map(s)).ToList();
           
            return View(vm);
        }

        // GET: Samourais/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Samourai samourai = db.Samourais.Find(id);
            if (samourai == null)
            {
                return HttpNotFound();
            }
            return View(samourai);
        }

        // GET: Samourais/Create
        public ActionResult Create()
        {
            CreateEditSamouraiViewModel vm = new CreateEditSamouraiViewModel();
            vm.FillListes(this.db);
            return View(vm);
        }

        // POST: Samourais/Create
        // Afin de déjouer les attaques par survalidation, activez les propriétés spécifiques auxquelles vous voulez établir une liaison. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateEditSamouraiViewModel vm)
        {
            if (ModelState.IsValid)
            {
                Samourai samourai = new Samourai();

                this.samouraisMapper.Map(vm, samourai);

                db.Samourais.Add(samourai);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            vm.FillListes(this.db);
            return View(vm);
        }

        // GET: Samourais/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Samourai samourai = db.Samourais.Find(id);
            if (samourai == null)
            {
                return HttpNotFound();
            }

            CreateEditSamouraiViewModel vm = new CreateEditSamouraiViewModel();
            this.samouraisMapper.Map(samourai, vm);

            vm.FillListes(this.db);

            return View(vm);
        }

        // POST: Samourais/Edit/5
        // Afin de déjouer les attaques par survalidation, activez les propriétés spécifiques auxquelles vous voulez établir une liaison. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CreateEditSamouraiViewModel vm)
        {
            if (ModelState.IsValid)
            {
                Samourai model = this.db.Samourais.Find(vm.Id);
                if (model == null)
                {
                    return HttpNotFound();
                }

                // Chargement explicite des arts martiaux
                this.db.Entry(model).Collection(s => s.ArtMartiaux).Load();

                this.samouraisMapper.Map(vm, model);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            vm.FillListes(this.db);
            return View(vm);
        }

        // GET: Samourais/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Samourai samourai = db.Samourais.Find(id);
            if (samourai == null)
            {
                return HttpNotFound();
            }
            return View(samourai);
        }

        // POST: Samourais/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Samourai samourai = db.Samourais.Find(id);
            db.Samourais.Remove(samourai);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
