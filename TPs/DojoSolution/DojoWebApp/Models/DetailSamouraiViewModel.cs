﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace DojoWebApp.Models
{
    public class DetailSamouraiViewModel : SamouraiViewModel
    {
        [DisplayName("Arme")]
        public string ArmeName { get; set; }

        [DisplayName("Arts martiaux maitrîsés")]
        public string ArtMartiauxNames { get; set; }
    }
}