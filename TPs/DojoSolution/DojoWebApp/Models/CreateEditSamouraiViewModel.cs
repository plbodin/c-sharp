﻿using BO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DojoWebApp.Models
{
    public class CreateEditSamouraiViewModel : SamouraiViewModel
    {
        [DisplayName("Arme")]
        public int? ArmeId { get; set; }
        public IEnumerable<SelectListItem> Armes { get; set; }

        [DisplayName("Art martiaux")]
        public int[] ArtMartiauxId { get; set; } = new int[0] { };

        public IEnumerable<SelectListItem> ArtMartiaux { get; set; }

        public void FillListes(DojoContext dojoContext)
        {
            this.Armes = dojoContext.Armes.Select(a => new SelectListItem
            {
                Text = a.Nom,
                Value = a.Id.ToString(),
                // On désactive les armes qui sont déjà associées a des Samourais
                // On ne désactive pas l'arme actuelle du SamouraiViewModel
                Disabled = a.Samourai != null && a.Samourai.Id != this.Id
            });

            this.ArtMartiaux = dojoContext.ArtMartials.Select(a => new SelectListItem
            {
                Text = a.Nom,
                Value = a.Id.ToString()
            });
        }
    }
}