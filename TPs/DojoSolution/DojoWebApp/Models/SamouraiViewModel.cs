﻿using BO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DojoWebApp.Models
{
    public class SamouraiViewModel
    {
        public int Id { get; set; }
        public int Force { get; set; }
        public string Nom { get; set; }

        public int Potentiel { get; set; }
    }
}