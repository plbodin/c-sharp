﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DojoWebApp.Extensions
{
    public static class HtmlExtensions
    {
        public static HtmlString PreviousPageLink(this HtmlHelper helper, string controllerName)
        {
            return new HtmlString($"<a class='btn btn-default' href='/{controllerName}'>Retour à la liste</a>");
        }
    }
}