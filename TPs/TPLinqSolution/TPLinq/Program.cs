﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TpLinq.BO;

namespace TPLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            // Pour avoir le symbole € qui s'affiche bien dans la console
            Console.OutputEncoding = System.Text.Encoding.Unicode;

            InitialiserDatas();

            // Afficher la liste des prénoms des auteurs dont le nom commence par "G"
            IEnumerable<string> prenomsAuteursLettreG;

            prenomsAuteursLettreG = ListeAuteurs.Where(a => a.Nom != null && a.Nom.StartsWith("G"))
                                                .Select(a => a.Prenom + " " + a.Nom);

            Console.WriteLine("Prénoms commencçant par la lettre G : " + string.Join(", ", prenomsAuteursLettreG));

            prenomsAuteursLettreG = from a in ListeAuteurs
                                    where a.Nom != null && a.Nom.StartsWith("G")
                                    select a.Prenom + " " + a.Nom;

            // Afficher l’auteur ayant écrit le plus de livres            
            IGrouping<Auteur, Livre> grouping = ListeLivres.GroupBy(l => l.Auteur)
                                    .OrderByDescending(g => g.Count())
                                    .FirstOrDefault();

            Console.WriteLine("Auteur ayant écrit le plus de livres : {0} ({1})", grouping.Key, grouping.Count());

            // Afficher le nombre moyen de pages par livre par auteur            
            IEnumerable<string> nombresMoyensParAuteur = ListeAuteurs.Select(a => new
            {
                Livres = ListeLivres.Where(l => l.Auteur == a),
                Auteur = a
            }).Select(s => $"{s.Auteur} : {s.Livres.DefaultIfEmpty().Average(l => l?.NbPages)} page(s)");


            nombresMoyensParAuteur = ListeAuteurs
                .Select((Auteur a) => $"{a}, moyenne : {ListeLivres.Where(l => l.Auteur == a).DefaultIfEmpty().Average(l => l?.NbPages)}");

            Console.WriteLine(
                "Liste des auteurs et leur moyenne de page par livre :\n" + string.Join("\n", nombresMoyensParAuteur)
                );


            // Afficher le titre du livre avec le plus de pages
            Livre livreAvecLePlusDePages = ListeLivres.OrderByDescending(l => l.NbPages).FirstOrDefault();

            Console.WriteLine("Livre avec le plus de pages : {0} ({1})", livreAvecLePlusDePages, livreAvecLePlusDePages.NbPages);

            // Afficher combien ont gagné les auteurs en moyenne (moyenne des factures)
            decimal moyenne = ListeAuteurs.Average(a => a.Factures.Sum(f => f.Montant));

            Console.WriteLine("Combien ont gagné les auteurs en moyenne ? {0:C2}", moyenne);

            // Afficher les auteurs et la liste de leurs livres
            List<string> auteursEtLeursLivres =
                 ListeAuteurs.Select(a => $"{a}: {string.Join(", ", ListeLivres.Where(l => l.Auteur == a).Select(l => l.Titre))}")
                 .ToList();

            Console.WriteLine("\nAuteurs et leurs livres : \n{0}", string.Join("\n", auteursEtLeursLivres));

            // Afficher les titres de tous les livres triés par ordre alphabétique
            Console.WriteLine("\nListe des livres triés ({1}):\n{0}",
                string.Join("\n", ListeLivres.OrderBy(l => l.Titre).ThenBy(l => l.Synopsis)),
                ListeLivres.Count
                );

            // Afficher la liste des livres dont le nombre de pages est supérieur à la moyenne
            var moyenneDesPages = ListeLivres.Count == 0 ? 0 : ListeLivres.Average(l => l.NbPages);
            Console.WriteLine(
                "\nLivres dont le nombre de pages est > à la moyenne ({1:F0})\n:{0}",
               string.Join("\n", ListeLivres.Where(l => l.NbPages > moyenneDesPages)),
               moyenneDesPages
                );

            // Afficher l'auteur ayant écrit le moins de livres
            Auteur auteurAyantEcrisLeMoinsDeLivre =
                 ListeAuteurs.OrderBy(a => ListeLivres.Where(l => l.Auteur == a).Count())
                             .FirstOrDefault();

            Console.WriteLine("\nAuteur ayant écrit le moins de livres : {0}", auteurAyantEcrisLeMoinsDeLivre);
            Console.ReadLine();
        }

        private static List<Auteur> ListeAuteurs = new List<Auteur>();
        private static List<Livre> ListeLivres = new List<Livre>();

        private static void InitialiserDatas()
        {
            ListeAuteurs.Add(new Auteur("GROUSSARD", "Thierry"));
            ListeAuteurs.Add(new Auteur("GABILLAUD", "Jérôme"));
            ListeAuteurs.Add(new Auteur("HUGON", "Jérôme"));
            ListeAuteurs.Add(new Auteur("ALESSANDRI", "Olivier"));
            ListeAuteurs.Add(new Auteur("de QUAJOUX", "Benoit"));
            ListeLivres.Add(new Livre(1, "C# 4", "Les fondamentaux du langage", ListeAuteurs.ElementAt(0), 533));
            ListeLivres.Add(new Livre(2, "VB.NET", "Les fondamentaux du langage", ListeAuteurs.ElementAt(0), 539));
            ListeLivres.Add(new Livre(3, "SQL Server 2008", "SQL, Transact SQL", ListeAuteurs.ElementAt(1), 311));
            ListeLivres.Add(new Livre(4, "ASP.NET 4.0 et C#", "Sous visual studio 2010", ListeAuteurs.ElementAt(3), 544));
            ListeLivres.Add(new Livre(5, "C# 4", "Développez des applications windows avec visual studio 2010", ListeAuteurs.ElementAt(2), 452));
            ListeLivres.Add(new Livre(6, "Java 7", "les fondamentaux du langage", ListeAuteurs.ElementAt(0), 416));
            ListeLivres.Add(new Livre(7, "SQL et Algèbre relationnelle", "Notions de base", ListeAuteurs.ElementAt(1), 216));
            ListeAuteurs.ElementAt(0).addFacture(new Facture(3500, ListeAuteurs.ElementAt(0)));
            ListeAuteurs.ElementAt(0).addFacture(new Facture(3200, ListeAuteurs.ElementAt(0)));
            ListeAuteurs.ElementAt(1).addFacture(new Facture(4000, ListeAuteurs.ElementAt(1)));
            ListeAuteurs.ElementAt(2).addFacture(new Facture(4200, ListeAuteurs.ElementAt(2)));
            ListeAuteurs.ElementAt(3).addFacture(new Facture(3700, ListeAuteurs.ElementAt(3)));
        }
    }
}
