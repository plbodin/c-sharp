﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formes
{
    public abstract class Forme
    {
        public abstract double Aire { get; }

        public abstract double Perimetre { get; }

        public override string ToString()
        {
            return string.Format("\nAire={0}\nPérimètre={1}\n", this.Aire, this.Perimetre);
        }

    }
}
