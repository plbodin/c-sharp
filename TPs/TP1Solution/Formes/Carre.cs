﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formes
{
    public class Carre : Forme
    {
        public double Longueur { get; set; }

        public override double Aire => this.Longueur * this.Longueur;

        public override double Perimetre => this.Longueur * 4;

        public override string ToString()
        {
            return this.OverrideToString() + base.ToString();
        }

        public virtual string OverrideToString()
        {
            return string.Format("Carré de côté {0}", this.Longueur);
        }
    }
}
