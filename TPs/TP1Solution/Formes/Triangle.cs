﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formes
{
    public class Triangle : Forme
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }

        public override double Aire
        {
            get
            {
                var p = (A + B + C) / 2;
                return Math.Sqrt((p - A) * (p - B) * (p - C) * p);
            }
        }

        public override double Perimetre => A + B + C;

        public override string ToString()
        {
            return string.Format("Triangle de côté A={0}, B={1} et C={2}.", this.A, this.B, this.C) + base.ToString();
        }
    }
}
