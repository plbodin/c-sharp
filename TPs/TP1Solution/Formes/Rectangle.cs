﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formes
{
    public class Rectangle : Carre
    {
        public double Largeur { get; set; }

        public override double Aire => this.Longueur * this.Largeur;
        public override double Perimetre => (this.Longueur + this.Largeur) * 2;

        public override string OverrideToString()
        {
            return string.Format("Rectangle de longueur {0} et de largeur {1}.", this.Longueur, this.Largeur);
        }
    }
}
