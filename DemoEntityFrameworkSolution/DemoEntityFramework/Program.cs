﻿using BO;
using BO.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DemoEntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;

            // Using => permets de disposer les ressources même si des erreurs ont lieu
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    Console.WriteLine("Succès de la connection à la base de données.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Échec de la connexion à la base de données.");
                }
            }

            // Lazy loading activé
            using (MonContext monContext = new MonContext(connectionString, true))
            {
                Pizza pizzaToto = monContext.Pizzas.Include(p => p.Ingredients).FirstOrDefault(p => p.Nom == "Totoro");
                List<Ingredient> ingredients = pizzaToto.Ingredients;

                Console.WriteLine("{0} ingrédients chargés.", ingredients?.Count);
            }

            // Lazy loading désactivé
            using (MonContext monContext = new MonContext(connectionString, false))
            {
                Pizza pizzaToto = monContext.Pizzas.FirstOrDefault(p => p.Nom == "Totoro");
                List<Ingredient> ingredients = pizzaToto.Ingredients;

                Console.WriteLine("{0} ingrédients chargés.", ingredients?.Count ?? 0);
            }

            // Lazy loading désactivé et EagerLoading
            using (MonContext monContext = new MonContext(connectionString, false))
            {
                Pizza pizzaToto = monContext.Pizzas.Include(p => p.Ingredients).FirstOrDefault(p => p.Nom == "Totoro");
                List<Ingredient> ingredients = pizzaToto.Ingredients;

                Console.WriteLine("{0} ingrédients chargés.", ingredients?.Count);
            }

            // Lazy loading désactivé et ExplicitLoading
            using (MonContext monContext = new MonContext(connectionString, false))
            {
                Pizza pizzaToto = monContext.Pizzas.FirstOrDefault(p => p.Nom == "Totoro");

                // Chargement des ingrédients
                monContext.Entry(pizzaToto).Collection(p => p.Ingredients).Load();

                // monContext.Ingredients
                List<Ingredient> ingredients = pizzaToto.Ingredients;

                Console.WriteLine("{0} ingrédients chargés.", ingredients?.Count);
            }

            // Jouons avec les states
            using (MonContext monContext = new MonContext(connectionString, true))
            {
                Pizza pizzaToto = monContext.Pizzas.FirstOrDefault(p => p.Nom == "Totoro");

                // State = unchanged
                Console.WriteLine("State de la pizza : {0}", monContext.Entry(pizzaToto).State);

                pizzaToto.Nom = "Toto";

                // State = modified
                Console.WriteLine("State de la pizza : {0}", monContext.Entry(pizzaToto).State);

                monContext.Pizzas.Remove(pizzaToto);

                // State = deleted
                Console.WriteLine("State de la pizza : {0}", monContext.Entry(pizzaToto).State);


            }

            Console.ReadLine();
        }
    }
}
