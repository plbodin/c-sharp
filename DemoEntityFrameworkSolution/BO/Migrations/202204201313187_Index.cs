﻿namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Index : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Pizza", "Nom");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Pizza", new[] { "Nom" });
        }
    }
}
