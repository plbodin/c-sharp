﻿namespace BO.Migrations
{
    using BO.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BO.MonContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BO.MonContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            string[] ingredients = new[]
            {
                "Champignons",
                "Tomates",
                "Jambon",
                "Poivrons",
                "Fromages",
                "Olives"
            };

            for (int i = 0; i < ingredients.Length; i++)
            {
                string ingredient = ingredients[i];

                // Récupération de l'ingrédient en base
                Ingredient persisted = context.Ingredients.FirstOrDefault(ing => ing.Nom == ingredient);

                if (persisted == null)
                {
                    // Ajout en base d'un ingrédient pour chaque ingrédient (string)
                    context.Ingredients.Add(new Ingredient { Nom = ingredient });
                }
            }
        }
    }
}
