﻿namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmptyForSeed : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.IngredientPizzas", newName: "PizzaIngredients");
            DropPrimaryKey("dbo.PizzaIngredients");
            AddPrimaryKey("dbo.PizzaIngredients", new[] { "Pizza_Id", "Ingredient_Id" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.PizzaIngredients");
            AddPrimaryKey("dbo.PizzaIngredients", new[] { "Ingredient_Id", "Pizza_Id" });
            RenameTable(name: "dbo.PizzaIngredients", newName: "IngredientPizzas");
        }
    }
}
