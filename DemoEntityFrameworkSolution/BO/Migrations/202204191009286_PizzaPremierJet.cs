﻿namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PizzaPremierJet : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pizza",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false, maxLength: 155),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pizza");
        }
    }
}
