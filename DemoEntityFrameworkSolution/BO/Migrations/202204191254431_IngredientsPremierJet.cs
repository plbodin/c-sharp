﻿namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IngredientsPremierJet : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ingredients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IngredientPizzas",
                c => new
                    {
                        Ingredient_Id = c.Int(nullable: false),
                        Pizza_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Ingredient_Id, t.Pizza_Id })
                .ForeignKey("dbo.Ingredients", t => t.Ingredient_Id, cascadeDelete: true)
                .ForeignKey("dbo.Pizza", t => t.Pizza_Id, cascadeDelete: true)
                .Index(t => t.Ingredient_Id)
                .Index(t => t.Pizza_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IngredientPizzas", "Pizza_Id", "dbo.Pizza");
            DropForeignKey("dbo.IngredientPizzas", "Ingredient_Id", "dbo.Ingredients");
            DropIndex("dbo.IngredientPizzas", new[] { "Pizza_Id" });
            DropIndex("dbo.IngredientPizzas", new[] { "Ingredient_Id" });
            DropTable("dbo.IngredientPizzas");
            DropTable("dbo.Ingredients");
        }
    }
}
