﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public class MonContext : DbContext
    {
        public DbSet<Pizza> Pizzas { get; set; }

        public DbSet<Ingredient> Ingredients { get; set; }

        public MonContext() : this(ConfigurationManager.ConnectionStrings["default"].ConnectionString, false)
        {
        }

        public MonContext(string connectionString, bool lazyLoading) : base(connectionString)
        {
            this.Configuration.LazyLoadingEnabled = lazyLoading;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Création d'un index sur le nom de la pizza
            modelBuilder.Entity<Pizza>().HasIndex(p => p.Nom);
        }
    }
}
