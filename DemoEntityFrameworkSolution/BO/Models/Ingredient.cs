﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Ingredient
    {
        [Key]
        public int? Id { get; set; }

        public string Nom { get; set; }

        public virtual List<Pizza> Pizzas { get; set; }
    }
}
