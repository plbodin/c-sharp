﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    [Table("Pizza")]
    public class Pizza
    {
        [Key]
        public virtual int? Id { get; set; }

        [Required]
        [StringLength(155, MinimumLength = 5)]
        public virtual string Nom { get; set; }


        public virtual List<Ingredient> Ingredients { get; set; }
    }
}
