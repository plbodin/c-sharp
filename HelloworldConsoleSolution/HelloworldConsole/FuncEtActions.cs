﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloworldConsole
{
    public class FuncEtActions
    {
        public static void Demo()
        {
            Action<string> writeLine = s => Console.WriteLine(s);

            Func<string> getString = () => "Toto";
            Func<string> getString2 = () =>
            {
                return "Toto";
            };

            Func<int, int, string> parseToString = (int i, int i2) => "" + i + i2;

            // Appel de la fonction getString
            writeLine(getString());
            writeLine(getString2());
            writeLine(parseToString(50, 20));

            Action a = () =>
            {
                writeLine("Execute action");
            };

            // Appelle l'Action a
            a();

            Action action = new Action(() =>
            {
                Console.WriteLine("toto");
            });
        }

        public void Toto(Func<int, string> filtre)
        {

        }
    }
}
