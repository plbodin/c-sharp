﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Extensions
{
    /// <summary>
    /// Une classe d'extension doit être static
    /// </summary>
    public static class StringExtension
    {

        public static string AddENIBefore(this string s)
        {
            return "ENI" + s;
        }
    }
}
