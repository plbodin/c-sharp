﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public class Ballon : Jouet
    {
        public override void Jouer()
        {
            Console.WriteLine("Je joue avec le ballon.");
        }

        public override string Couleur()
        {
            return "Rouge";
        }                
    }
}
