﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public class KeyValue<K, V>
    {
        public K Key { get; set; }

        public V Value { get; set; }


        public static void PrintValues()
        {
            KeyValue<string, int> kv = new KeyValue<string, int>();
            kv.Key = "Clé1";
            kv.Value = 1;

            KeyValue<double, string> kv2 = new KeyValue<double, string>();
            kv2.Key = 5;
            kv2.Value = "Valeur";
        }
    }
}
